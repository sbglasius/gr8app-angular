angular.module('gr8appApp')
.directive('agendaItem', function() {
    return {
        restrict: "E",
        replace: true,
        templateUrl: 'views/partials/directives/_agendaItem.html',
        scope: {
            item: '=',
            startHour: '=',
            color: '=',
            room: '='
        },
        controller: ['$scope','scrollService',function($scope, scrollService) {
            var item = $scope.item;
            $scope.topValue = (convertTimeToInteger(item.start) - $scope.startHour + 1) * 60;
            $scope.height = (convertTimeToInteger(item.end) - convertTimeToInteger(item.start+1)) * 60;
            $scope.small = $scope.height < 20 ? 'small-row small':'';
            $scope.break = $scope.height > 20;
            $scope.start = moment(item.start, 'HH:mm:ss').format('HH:mm');
            $scope.end = moment(item.end, 'HH:mm:ss').format('HH:mm');
            $scope.scrollTo = scrollService.scrollTo;
            $scope.showRoom = $scope.room != undefined && $scope.room != "";
        }]
    };
});
function convertTimeToInteger(time) {
    return parseInt(moment(time, 'HH:mm:ss').format('HH')) +
        parseInt(moment(time, 'HH:mm:ss').format('mm'))/60
}
