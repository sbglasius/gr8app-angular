angular.module('gr8appApp')
.directive('speaker', function() {
    return {
        restrict: "E",
        replace: false,
        templateUrl: 'views/partials/directives/_speaker.html',
        scope: {
            speaker: '='
        },
        controller: ['$scope','scrollService',function($scope, scrollService) {
            $scope.scrollTo = scrollService.scrollTo
        }]
    };
});
