angular.module('gr8appApp')
.directive('talk', function() {
    return {
        restrict: "E",
        replace: true,
        templateUrl: 'views/partials/directives/_talk.html',
        scope: {
            talk: '='
        },
        controller: ['$scope','scrollService',function($scope, scrollService) {
            $scope.scrollTo = scrollService.scrollTo
        }]
    };
});
