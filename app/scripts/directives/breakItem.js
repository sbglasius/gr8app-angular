angular.module('gr8appApp')
.directive('breakItem', function() {
    return {
        restrict: "E",
        replace: true,
        templateUrl: 'views/partials/directives/_breakItem.html',
        scope: {
            item: '=',
            startHour: '=',
            color: '='
        },
        controller: ['$scope',function($scope) {
            var item = $scope.item;
            $scope.topValue = (convertTimeToInteger(item.start) - $scope.startHour + 1) * 60;
            $scope.height = (convertTimeToInteger(item.end) - convertTimeToInteger(item.start+1)) * 60;
        }]
    };
}).directive('breakIcon', function() {
    return {
        restrict: "E",
        replace: true,
        templateUrl: 'views/partials/directives/_breakIcon.html',
        scope: {
            item: '=',
            startHour: '=',
            color: '='
        },
        controller: ['$scope',function($scope) {
            var item = $scope.item;
            item.icon = item.icon.replace('food','cutlery').replace('icon','fa');
            $scope.topValue = (convertTimeToInteger(item.start) - $scope.startHour + 1) * 60;
            $scope.height = (convertTimeToInteger(item.end) - convertTimeToInteger(item.start+1)) * 60;
        }]
    };
});
function convertTimeToInteger(time) {
    return parseInt(moment(time, 'HH:mm:ss').format('HH')) +
        parseInt(moment(time, 'HH:mm:ss').format('mm'))/60
}
