angular.module('gr8appApp')
.directive('crew', function() {
    return {
        restrict: "E",
        replace: false,
        templateUrl: 'views/partials/directives/_crew.html',
        scope: {
            item: '='
        },
        controller: ['$scope',function($scope) {
        }]
    };
});
