'use strict';

var app = angular.module('gr8appApp', [
    'ngCookies',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'duScroll',
    'angularytics',
    'config',
    'ui.bootstrap',
    'angularMoment'
]).config(['$routeProvider','AngularyticsProvider',function ($routeProvider, AngularyticsProvider) {
    AngularyticsProvider.setEventHandlers(['Console', 'GoogleUniversal']);

        $routeProvider
          .when('/:anchor', {
            templateUrl: 'views/main.html',
            controller: 'MainCtrl'
          })
          .otherwise({
            redirectTo: '/'
          });
}]).run(['Angularytics','backendService', function (Angularytics, backendService) {
    Angularytics.init();
    OAuth.initialize('1WAuaxiRscO6Q5IP9Yzt980z2dI');
    backendService.bootstrap();

}]);

var underscore = angular.module('underscore', []);
underscore.factory('_', function() {
    return window._; // assumes underscore has already been loaded on the page
});