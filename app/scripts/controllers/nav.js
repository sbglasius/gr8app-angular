'use strict';

app
    .controller('NavCtrl', ['$scope','scrollService','registerService', function ($scope, scrollService, registerService) {
        $scope.scrollTo = scrollService.scrollTo;
        $scope.registerClick = registerService.registerClick;
    }]);


    
    
