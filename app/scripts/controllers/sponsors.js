'use strict';

app
    .controller('SponsorsCtrl', ['$scope', 'modalService', function ($scope, modalService) {
        $scope.showSponsor = function (sponsor) {
            modalService.showSponsor(sponsor)
        }
    }]);
