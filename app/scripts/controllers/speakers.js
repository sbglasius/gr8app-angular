app
    .controller('SpeakerCtrl', ['$scope', 'dataService', 'FEATURES',  function ($scope, dataService, FEATURES) {
        $scope.$on('loaded', function(event, args) {
            if (FEATURES.speakers && args[0] === "speakers") {
                var n = 3;
                var speakers = _.chain(args[1]).groupBy(function(element, index){
                    return Math.floor(index/n);
                }).toArray().value();
                $scope.speakers = speakers;
            }
        })
    }]);
