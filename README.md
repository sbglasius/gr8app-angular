To get started, you may have to run npm install.

## Configuration
Configuration is located in config.json at the root of the project.  Environments production and development exist right now.  

The development environment is used when running ```grunt serve```, 
the production environment is used when creating a distribution using ```grunt build```

When running grunt serve, a file called config.js is created in the scripts directory.  **DO NOT modify this file**.  
It is regenerated based on config.json, so any changes you make will be overwritten.  All configuration should be added to config.json.

Items in the configuration are converted into angular constants and can be injected.

It should be noted that sensitive items shouldn't be hidden behind this configuration, since it's simply disabled at the client side.

